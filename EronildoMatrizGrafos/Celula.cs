﻿public class Celula {
    private string _id;
    private int _linha, _coluna, _valor;
    private string _idEsquerda, _idDireita, _idAcima, _idAbaixo;
    private bool _isAtiva;

    public Celula(int linha, int coluna) {
        _linha = linha;
        _coluna = coluna;

        SetupCelula();
    }

    private void SetupCelula() {
		SetId();
        SetValor();

		SetIdCelulasAoRedor(CelulasAoRedor.DIREITA);
		SetIdCelulasAoRedor(CelulasAoRedor.ESQUERDA);
		SetIdCelulasAoRedor(CelulasAoRedor.ACIMA);
		SetIdCelulasAoRedor(CelulasAoRedor.ABAIXO);
    }

	private void SetId()
	{
		_id = Util.GetIdCelula(_linha, _coluna);
	}

    private void SetValor() {
        _valor = Util.GetValor();
        SetIsAtiva(_valor != 0);
    }

    public void SetValor(int valor)
    {
        _valor = valor;
        SetIsAtiva(_valor != 0);
    }

    public void SetIsAtiva(bool ativa)
    {
        _isAtiva = ativa;
    }

    public bool IsAtiva()
    {
        return _isAtiva;
    }

    private void SetIdCelulasAoRedor(CelulasAoRedor aoRedor) {

		var linha = _linha;
		var coluna = _coluna;

        switch (aoRedor)
        {
            case CelulasAoRedor.DIREITA:
				coluna = _coluna + 1;
                _idDireita = (coluna < Util.MAX_COLUNAS) ?
                    Util.GetIdCelula(linha, coluna) : string.Empty;
                break;
            case CelulasAoRedor.ESQUERDA:
				coluna = _coluna - 1;
				_idEsquerda = (coluna > 0) ?
					Util.GetIdCelula(linha, coluna) : string.Empty;
				break;
            case CelulasAoRedor.ACIMA:
                linha = _linha + 1;
                _idAcima = (linha < Util.MAX_LINHAS) ?
					Util.GetIdCelula(linha, coluna) : string.Empty;
				break;
            case CelulasAoRedor.ABAIXO:
                linha = _linha - 1;
                _idAbaixo = (linha > 0) ?
					Util.GetIdCelula(linha, coluna) : string.Empty;
				break;
        }
    }

	public string GetId()
	{
		return _id;
	}

	public int GetValor()
	{
		return _valor;
	}

    public int GetLinha()
    {
        return _linha;
    }

    public int GetColuna()
    {
        return _coluna;
    }

    public string GetIdAoRedor(CelulasAoRedor aoRedor) {
        
        switch (aoRedor)
        {
			case CelulasAoRedor.DIREITA:
                return _idDireita;
			case CelulasAoRedor.ESQUERDA:
                return _idEsquerda;
			case CelulasAoRedor.ACIMA:
                return _idAcima;
			case CelulasAoRedor.ABAIXO:
                return _idAbaixo;
        }

        return string.Empty;
    }
}