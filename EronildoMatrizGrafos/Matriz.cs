﻿public class Matriz {

    private Celula[] _celulas;

    public Matriz()
    {
        _celulas = new Celula[Util.MAX_LINHAS * Util.MAX_COLUNAS];
    }

    public Celula[] GerarMatriz() {
        _celulas = new Celula[Util.MAX_LINHAS * Util.MAX_COLUNAS];

        for (int l = 0; l < Util.MAX_LINHAS; l++)
            for (int c = 0; c < Util.MAX_COLUNAS; c++)
                _celulas[Util.GeneratePosition(l, c)] = new Celula(l, c);

        return GetCelulas();
    }

    public Celula[] GetCelulas() {
        return _celulas;
    }

    public Celula GetCelula(int linha, int coluna)
    {
        Celula celula = null;
        try
        {
            celula = _celulas[Util.GeneratePosition(linha, coluna)];
        }
        catch (System.Exception) {}
        
        return celula;
    }

    public Celula GetCelulaById(string id)
    {
        if (string.IsNullOrEmpty(id))
            return null;

        var lados = id.Replace('M', ' ').Trim().Split(',');
        var linha = int.Parse(lados[0]);
        var coluna = int.Parse(lados[1]);

        return GetCelula(linha, coluna);
    }
}