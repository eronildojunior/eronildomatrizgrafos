﻿public static class Util {
	public const int MAX_LINHAS = 10, MAX_COLUNAS = 10;

    public static string GetIdPrimeiraCelula()
    {
        return GetIdCelula(0, 0);
    }

    public static string GetIdUltimaCelula()
    {
        return GetIdCelula(MAX_LINHAS - 1, MAX_COLUNAS - 1);
    }

    private static System.Random _random = new System.Random();

    public static int GetValor() {
        return _random.Next(1, 100) <= 30 ? 0 : 1;
    }

    public static string GetIdCelula(int linha, int coluna) {
        return "M" + linha + "," + coluna;
    }

    public static int GeneratePosition(int linha, int coluna)
    {
        var posString = linha.ToString() + coluna.ToString();
        var pos = 0;

        int.TryParse(posString, out pos);

        return pos;
    }

    public static bool IsCelulaInicial(Celula celula)
    {
        if (celula.GetLinha() == 0 && celula.GetColuna() == 0)
            return true;
        return false;
    }

    public static bool IsCelulaFinal(Celula celula)
    {
        if (celula.GetLinha() == (MAX_LINHAS - 1) && 
            celula.GetColuna() == (MAX_COLUNAS - 1))
            return true;
        return false;
    }
}

public enum CelulasAoRedor {
    DIREITA,
    ESQUERDA,
    ACIMA,
    ABAIXO
}