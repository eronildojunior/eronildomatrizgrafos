﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace EronildoMatrizGrafos
{
    public class Car
    {
        private string _currentTextId;
        private Image _imgCar;
        private Dictionary<string, TextBlock> _textCelulas;

        public Car(Image imgCar)
        {
            _currentTextId = "0,0";
            _imgCar = imgCar;
            _textCelulas = new Dictionary<string, TextBlock>();
        }

        public void AddTextCelula(string id, TextBlock textCelula)
        {
            _textCelulas.Add(id, textCelula);
        }

        public void ClearTextCelula()
        {
            _textCelulas.Clear();
        }

        public void TryMoveCarToUp()
        {
            GoToTextTarget(CelulasAoRedor.ACIMA);
        }

        public void TryMoveCarToDown()
        {
            GoToTextTarget(CelulasAoRedor.ABAIXO);
        }

        public void TryMoveCarToRight()
        {
            GoToTextTarget(CelulasAoRedor.DIREITA);
        }

        public void TryMoveCarToLeft()
        {
            GoToTextTarget(CelulasAoRedor.ESQUERDA);
        }

        private void GoToTextTarget(CelulasAoRedor direcao)
        {
            var lados = _currentTextId.Split(',');
            var linha = int.Parse(lados[0]);
            var coluna = int.Parse(lados[1]);

            var id = string.Empty;

            switch (direcao)
            {
                case CelulasAoRedor.DIREITA:
                    id = linha.ToString() + ',' + (coluna + 1).ToString();
                    break;
                case CelulasAoRedor.ESQUERDA:
                    id = linha.ToString() + ',' + (coluna - 1).ToString();
                    break;
                case CelulasAoRedor.ACIMA:
                    id = (linha - 1).ToString() + ',' + coluna.ToString();
                    break;
                case CelulasAoRedor.ABAIXO:
                    id = (linha + 1).ToString() + ',' + coluna.ToString();
                    break;
            }

            var textTarget = _textCelulas.ContainsKey(id) ? _textCelulas[id] : null;
            if (textTarget != null && textTarget.Text == "1")
            {
                var panel = (DockPanel)textTarget.Parent;
                DisconnectCarFromParent();
                panel.Children.Add(_imgCar);
                _currentTextId = id;
            }
        }

        public void DisconnectCarFromParent()
        {
            ((DockPanel)_imgCar.Parent).Children.Remove(_imgCar);
        }
    }
}
