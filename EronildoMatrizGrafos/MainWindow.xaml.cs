﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace EronildoMatrizGrafos
{
    public partial class MainWindow : Window
    {
        private Matriz _matriz;
        private Stack<Celula> _rota;
        private List<Border> _borderRotas;
        private Car _car;

        public MainWindow()
        {
            InitializeComponent();

            _rota = new Stack<Celula>();
            _matriz = new Matriz();
            _borderRotas = new List<Border>();
            CreateGrid();
        }

        #region Métodos

        private void CreateGrid()
        {
            _car = new Car(imgCar);
            _car.ClearTextCelula();
            grid.Children.Clear();
            _borderRotas.Clear();
            var celulas = _matriz.GerarMatriz();
            CalcularRotas();

            for (int l = 0; l < Util.MAX_LINHAS; l++)
                grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(40) });
            for (int c = 0; c < Util.MAX_COLUNAS; c++)
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(40) });

            foreach (var celula in celulas)
            {
                var dockPanel = new DockPanel();
                var valor = celula.GetValor();
                var text = new TextBlock();
                text.Text = valor.ToString();
                text.TextAlignment = TextAlignment.Center;
                text.VerticalAlignment = VerticalAlignment.Center;
                dockPanel.Children.Add(text);
                var border = new Border();
                var backgroundColor = Brushes.GhostWhite;
                var borderColor = Brushes.Gainsboro;

                if (celula.GetId() == Util.GetIdPrimeiraCelula())
                {
                    _car.DisconnectCarFromParent();
                    dockPanel.Children.Add(imgCar);
                    backgroundColor = Brushes.Yellow;
                    borderColor = Brushes.DarkGoldenrod;
                }
                else if (celula.GetId() == Util.GetIdUltimaCelula())
                {
                    backgroundColor = Brushes.Yellow;
                    borderColor = Brushes.DarkGoldenrod;
                }
                else if (valor == 0)
                {
                    backgroundColor = Brushes.LightSalmon;
                    borderColor = Brushes.Red;
                }
                else if (_rota.Count > 0 && _rota.Where(c => c == celula).FirstOrDefault() != null)
                    _borderRotas.Add(border);

                border.Background = backgroundColor;
                border.BorderBrush = borderColor;
                border.BorderThickness = new Thickness(0.5);
                var linha = celula.GetLinha();
                var coluna = celula.GetColuna();
                border.SetValue(Grid.RowProperty, linha);
                border.SetValue(Grid.ColumnProperty, coluna);
                border.Child = dockPanel;
                _car.AddTextCelula(linha.ToString() + ',' + coluna.ToString(), text);
                grid.Children.Add(border);
            }
        }

        #region Rota para Baixo

        private bool RotaAbaixo(Celula cursor, Celula cursorAnterior)
        {
            if (Util.IsCelulaFinal(cursor))
                return true;
            else if (_rota.Count == 0)
                return false;

            // vá para baixo
            var linha = cursor.GetLinha();
            var coluna = cursor.GetColuna();
            var celula = _matriz.GetCelula(linha + 1, coluna);
            if (celula != null && celula.IsAtiva() && celula != cursorAnterior)
            {
                // anda para a celula
                cursorAnterior = cursor;
                cursor = celula;

                if (_rota.Contains(celula))
                {
                    cursorAnterior.SetIsAtiva(false);
                    _rota.Pop();
                }
                else
                    _rota.Push(cursor);

                // vá para baixo
                return RotaAbaixo(cursor, cursorAnterior);
            }
            else
            {
                // se não vá para direita
                celula = _matriz.GetCelula(linha, coluna + 1);

                if (celula != null && celula.IsAtiva() && celula != cursorAnterior)
                {
                    // anda para a celula
                    cursorAnterior = cursor;
                    cursor = celula;

                    if (_rota.Contains(celula))
                    {
                        cursorAnterior.SetIsAtiva(false);
                        _rota.Pop();
                    }
                    else
                        _rota.Push(cursor);

                    // vá para baixo
                    return RotaAbaixo(cursor, cursorAnterior);
                }
                else
                {
                    // se não vá para esquerda
                    celula = _matriz.GetCelula(linha, coluna - 1);

                    if (celula != null && celula.IsAtiva() && celula != cursorAnterior)
                    {
                        if (RetornaSeEstaNaRota(celula, cursor))
                        {
                            return DisativaEVolta(cursor, cursorAnterior);
                        }
                        else
                        {
                            // anda para a celula
                            cursorAnterior = cursor;
                            cursor = celula;

                            if (_rota.Contains(celula))
                            {
                                cursorAnterior.SetIsAtiva(false);
                                _rota.Pop();
                            }
                            else
                                _rota.Push(cursor);

                            // vá para baixo
                            return RotaAbaixo(cursor, cursorAnterior);
                        }
                    }
                    else
                    {
                        // se não vá para cima
                        celula = _matriz.GetCelula(linha - 1, coluna);

                        if (celula != null && celula.IsAtiva() && celula != cursorAnterior)
                        {
                            if (RetornaSeEstaNaRota(celula, cursor))
                            {
                                return DisativaEVolta(cursor, cursorAnterior);
                            }
                            else
                            {
                                // anda para a celula
                                cursorAnterior = cursor;
                                cursor = celula;

                                if (_rota.Contains(celula))
                                {
                                    cursorAnterior.SetIsAtiva(false);
                                    _rota.Pop();
                                }
                                else
                                    _rota.Push(cursor);

                                // vá para baixo
                                return RotaAbaixo(cursor, cursorAnterior);
                            }
                        }
                        else
                        {
                            return DisativaEVolta(cursor, cursorAnterior);
                        }
                    }
                }
            }
        }

        #endregion

        private void CalcularRotas()
        {
            var celulaInicial = _matriz.GetCelula(0, 0);
            var celulaFinal = _matriz.GetCelula(Util.MAX_LINHAS - 1, Util.MAX_COLUNAS - 1);
            celulaInicial.SetValor(1);
            celulaFinal.SetValor(1);

            Celula cursorAnterior = null;
            Celula cursor = celulaInicial;

            _rota.Clear();
            _rota.Push(cursor);

            if (RotaAbaixo(cursor, cursorAnterior))
            {
                // adicionar rotas
                var rota = _rota;
            }
            else
            {
                _rota.Clear();
            }
        }

        // Se uma das adjacentes está ativa e é diferente do cursor e está na rota 
        private bool RetornaSeEstaNaRota(Celula celula, Celula cursor)
        {
            // abaixo
            var abaixoId = celula.GetIdAoRedor(CelulasAoRedor.ABAIXO);
            var abaixo = _matriz.GetCelulaById(abaixoId);
            if (EstaNaRota(abaixo, cursor))
                return true;

            // acima
            var acimaId = celula.GetIdAoRedor(CelulasAoRedor.ACIMA);
            var acima = _matriz.GetCelulaById(acimaId);
            if (EstaNaRota(acima, cursor))
                return true;

            // esquerda
            var esquerdaId = celula.GetIdAoRedor(CelulasAoRedor.ESQUERDA);
            var esquerda = _matriz.GetCelulaById(esquerdaId);
            if (EstaNaRota(esquerda, cursor))
                return true;

            // direita
            var direitaId = celula.GetIdAoRedor(CelulasAoRedor.DIREITA);
            var direita = _matriz.GetCelulaById(direitaId);
            if (EstaNaRota(direita, cursor))
                return true;

            return false;
        }

        private bool EstaNaRota(Celula adjacente, Celula cursor)
        {
            return (adjacente != null &&
                    adjacente.IsAtiva() &&
                    adjacente != cursor &&
                    _rota.Contains(adjacente));
        }

        private bool DisativaEVolta(Celula cursor, Celula cursorAnterior)
        {
            // se não desativa
            cursor.SetIsAtiva(false);
            _rota.Pop();

            if (Util.IsCelulaInicial(cursor))
            {
                // Sem rota.
                return false;
            }
            else
            {
                var cursorTemp = cursor;

                if (cursorAnterior.IsAtiva())
                    cursor = cursorAnterior;
                else if (_rota.Count > 0)
                    cursor = _rota.Peek();

                cursorAnterior = cursorTemp;

                // vá para baixo
                return RotaAbaixo(cursor, cursorAnterior);
            }
        }

        #endregion

        #region Events

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CreateGrid();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            foreach (var border in _borderRotas)
            {
                border.Background = Brushes.Lime;
                border.BorderBrush = Brushes.DarkGreen;
            }

            _borderRotas.Clear();
        }

        private void Image_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up:
                case Key.W:
                    btnW.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                    break;
                case Key.Left:
                case Key.A:
                    btnA.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                    break;
                case Key.Right:
                case Key.D:
                    btnD.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                    break;
                case Key.Down:
                case Key.S:
                    btnS.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                    break;
                default:
                    break;
            }
        }

        private void btnW_Click(object sender, RoutedEventArgs e)
        {
            _car.TryMoveCarToUp();
        }

        private void btnA_Click(object sender, RoutedEventArgs e)
        {
            _car.TryMoveCarToLeft();
        }

        private void btnS_Click(object sender, RoutedEventArgs e)
        {
            _car.TryMoveCarToDown();
        }

        private void btnD_Click(object sender, RoutedEventArgs e)
        {
            _car.TryMoveCarToRight();
        }

        #endregion

        #region Rota da Direita

        //private bool RotaDireita(Celula cursor, Celula cursorAnterior)
        //{
        //    if (Util.IsCelulaFinal(cursor))
        //        return true;
        //    else if (_rota.Count == 0)
        //        return false;

        //    // vá para a direita
        //    var linha = cursor.GetLinha();
        //    var coluna = cursor.GetColuna();
        //    var celula = _matriz.GetCelula(linha, coluna + 1);
        //    if (celula != null && celula.IsAtiva() && celula != cursorAnterior)
        //    {
        //        // anda para a celula
        //        cursorAnterior = cursor;
        //        cursor = celula;

        //        if (_rota.Contains(celula))
        //        {
        //            cursorAnterior.SetIsAtiva(false);
        //            _rota.Pop();
        //        }
        //        else
        //            _rota.Push(cursor);

        //        // vá para a direita
        //        return RotaDireita(cursor, cursorAnterior);
        //    }
        //    else
        //    {
        //        // se não vá para baixo
        //        celula = _matriz.GetCelula(linha + 1, coluna);

        //        if (celula != null && celula.IsAtiva() && celula != cursorAnterior)
        //        {
        //            // anda para a celula
        //            cursorAnterior = cursor;
        //            cursor = celula;

        //            if (_rota.Contains(celula))
        //            {
        //                cursorAnterior.SetIsAtiva(false);
        //                _rota.Pop();
        //            }
        //            else
        //                _rota.Push(cursor);

        //            // vá para a direita
        //            return RotaDireita(cursor, cursorAnterior);
        //        }
        //        else
        //        {
        //            // se não vá para esquerda
        //            celula = _matriz.GetCelula(linha, coluna - 1);

        //            if (celula != null && celula.IsAtiva() && celula != cursorAnterior)
        //            {
        //                if (RetornaSeEstaNaRota(celula, cursor))
        //                {
        //                    return DisativaEVolta(cursor, cursorAnterior);
        //                }
        //                else
        //                {
        //                    // anda para a celula
        //                    cursorAnterior = cursor;
        //                    cursor = celula;

        //                    if (_rota.Contains(celula))
        //                    {
        //                        cursorAnterior.SetIsAtiva(false);
        //                        _rota.Pop();
        //                    }
        //                    else
        //                        _rota.Push(cursor);

        //                    // vá para direita
        //                    return RotaDireita(cursor, cursorAnterior);
        //                }
        //            }
        //            else
        //            {
        //                // se não vá para cima
        //                celula = _matriz.GetCelula(linha - 1, coluna);

        //                if (celula != null && celula.IsAtiva() && celula != cursorAnterior)
        //                {
        //                    if (RetornaSeEstaNaRota(celula, cursor))
        //                    {
        //                        return DisativaEVolta(cursor, cursorAnterior);
        //                    }
        //                    else
        //                    {
        //                        // anda para a celula
        //                        cursorAnterior = cursor;
        //                        cursor = celula;

        //                        if (_rota.Contains(celula))
        //                        {
        //                            cursorAnterior.SetIsAtiva(false);
        //                            _rota.Pop();
        //                        }
        //                        else
        //                            _rota.Push(cursor);

        //                        // vá para direita
        //                        return RotaDireita(cursor, cursorAnterior);
        //                    }
        //                }
        //                else
        //                {
        //                    return DisativaEVolta(cursor, cursorAnterior);
        //                }
        //            }
        //        }
        //    }
        //}

        #endregion
    }
}